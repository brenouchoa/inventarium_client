# coding=utf-8
import json
import os
import shelve
import sqlite3
import tempfile
import tkFileDialog
import tkMessageBox
import urllib
from Tkinter import *
from datetime import datetime
from decimal import Decimal

import requests
from pytz import timezone

try:
    import win32api
    import win32print
except:
    pass

from dbfread import DBF


class Application(Frame):
    def lista_inventarios(self):
        try:
            url = "http://{}:8000/inventarios_abertos/".format(self.d['servidor'])
            response = urllib.urlopen(url)
            data = json.loads(response.read())
            # print data
            texto = ''
            for i in data['inventarios']:
                texto += 'Id: {} - Filial: {} - Data: {}\n'.format(
                    i['id'],
                    i['filial'],
                    i['data'],
                )

            tkMessageBox.showinfo("Inventários", texto)
        except IOError:
            tkMessageBox.showinfo("Erro", 'Servidor não encontrado.')

    def dados_inventario(self):
        try:
            url = "http://{}:8000/inventarios_abertos/".format(self.d['servidor'])
            response = urllib.urlopen(url)
            data = json.loads(response.read())
            # print data
            texto = ''
            ok = False
            id_inventario = self.id_inventario.get()
            for i in data['inventarios']:
                if str(id_inventario).strip() == str(i['id']).strip():
                    self.d['filial'] = i['filial']
                    self.lbl_filial['text'] = 'Filial: {}'.format(self.d['filial'])
                    self.d['data'] = i['data']
                    self.lbl_data['text'] = 'Data: {}'.format(self.d['data'])
                    self.d.sync()
                    ok = True
            if ok:
                tkMessageBox.showinfo("Ok", 'Dados Atualizados.')
            else:
                tkMessageBox.showinfo("Erro", 'Inventário {} não Encontrado.'.format(id_inventario))
        except IOError:
            tkMessageBox.showinfo("Erro", 'Servidor não encontrado.')

    def ler_cadastro(self):
        options = {'initialdir': 'C:\\', 'parent': root, 'title': 'Selecionar Arquivo do Cadastro'}
        arquivo = tkFileDialog.askopenfilename(**options)
        if arquivo:
            # with open(arquivo, 'r') as file:
            #     for linha in file:
            #         linha = linha.strip().split('\t')
            #         produtos.append([linha[0], linha[1], linha[2]])
            self.d['cadastro'] = arquivo
            self.lbl_cadastro['text'] = 'Arquivo Cadastro: {}'.format(self.d['cadastro'])
            self.d.sync()

    def definir_pasta(self):
        options = {'initialdir': 'C:\\', 'mustexist': True, 'parent': root, 'title': 'Selecionar Pasta do Inventário'}
        pasta = tkFileDialog.askdirectory(**options)
        if pasta:
            self.d['pasta'] = pasta
            self.d.sync()
            print self.d['pasta']
            self.ler_cadastro()
            self.atualizar()

    def atualizar(self):
        try:
            upload = {
                'id_inventario': self.d['id_inventario'],
                'id_base': self.d['id_base'],
            }
            self.d['num_coletores'] = 0
            self.d['num_pecas'] = 0
            self.d['num_secoes'] = 0
            self.lbl_num_coletores['text'] = 'Atualizando...'
            self.lbl_num_pecas['text'] = 'Atualizando...'
            self.lbl_num_secoes['text'] = 'Atualizando...'
            leituras = []
            coletores = []
            secoes = []
            for fn in os.listdir(self.d['pasta']):
                file = os.path.join(self.d['pasta'], fn)
                if os.path.isfile(file):
                    filename, file_extension = os.path.splitext(file)
                    if fn[:5].upper() == 'CONTA' and file_extension.upper() == '.DBF':
                        self.d['num_coletores'] += 1
                        num_coletor = fn[5:8]
                        print num_coletor
                        ordem = 0
                        num_pecas_coletor = 0
                        num_leituras_coletor = 0
                        for l in DBF(file):
                            ordem += 1
                            self.d['num_pecas'] += l['PECAS']
                            num_pecas_coletor += l['PECAS']
                            num_leituras_coletor += 1
                            leituras.append({
                                'num_coletor': num_coletor,
                                'ordem': ordem,
                                'num_secao': l['SECAO'],
                                'leitura': l['REF'],
                                'pecas': l['PECAS']}
                            )
                            if l['SECAO'] not in secoes:
                                secoes.append(l['SECAO'])
                                self.d['num_secoes'] += 1
                        coletores.append({
                            'num_coletor': num_coletor,
                            'num_pecas': num_pecas_coletor,
                            'num_leituras': num_leituras_coletor}
                        )

            self.d['leituras'] = leituras
            self.d['coletores'] = coletores
            self.d['secoes'] = secoes
            self.lbl_num_coletores['text'] = 'Número de Coletores: {}'.format(self.d['num_coletores'])
            self.lbl_num_pecas['text'] = 'Número de Pecas: {}'.format(self.d['num_pecas'])
            self.lbl_num_secoes['text'] = 'Número de Seções: {}'.format(self.d['num_secoes'])

            self.d.sync()
            r = requests.post("http://{}:8000/upload/".format(self.d['servidor']), json=upload)
            print(r.status_code)
            tkMessageBox.showinfo("Ok", 'Dados Atualizados.')

        except OSError:
            tkMessageBox.showinfo("Erro", 'Erro ao listar a pasta: {}'.format(self.d['pasta']))

    def get_dados_produto(self, c, leitura):
        c.execute('SELECT codigo_interno, descricao, preco FROM produtos WHERE codigo =?', [leitura])
        return c.fetchone()
        # for produto in self.d['produtos']:
        #     if produto[0] == leitura:
        #         return produto[1], produto[2]

    def imprimir_secao(self):
        inicial = int(self.secao_inicio.get())
        final = int(self.secao_fim.get() or self.secao_inicio.get())
        conn = sqlite3.connect(self.d['cadastro'])
        c = conn.cursor()
        tz = timezone('America/Sao_Paulo')
        fmt = '%d/%m/%Y %H:%M:%S'
        agora = tz.localize(datetime.now())
        agora = agora.strftime(fmt)
        for secao in range(inicial, final + 1):
            filename = tempfile.mktemp(".txt")
            total_valor = 0
            total_pecas = 0
            with open(filename, "w") as file:
                file.write('Inventarium - {} - {}{:>75}\r\n\r\n'.format(
                    self.d['filial'],
                    self.d['data'],
                    agora
                ))
                file.write('Seção: {} - Base:{}\r\n\r\n'.format(
                    secao,
                    self.d['id_base'],
                ))
                file.write('{:^9}{:^7}{:^15}{:^15}{:^43}{}{}{}\r\n'.format(
                    'Coletor',
                    'Ordem',
                    'Leitura',
                    'LM',
                    'Descricao',
                    '   Preço   ',
                    '   Peças   ',
                    '   Total   '
                ))
                file.write('{:^9}{:^7}{:^15}{:^15}{:^43}{:^11}{:^11}{:^11}\r\n'.format(
                    '-------',
                    '-----',
                    '-------------',
                    '-------------',
                    '-----------------------------------------',
                    '---------',
                    '---------',
                    '---------'
                ))
                print secao
                for leitura in self.d['leituras']:
                    if leitura['num_secao'] == str(secao):
                        print leitura
                        lm, descricao, preco = self.get_dados_produto(c, leitura['leitura'])
                        valor = round(Decimal(preco) * Decimal(leitura['pecas']), 2)
                        total_valor += valor
                        total_pecas += leitura['pecas']
                        file.write('{:^9}{:^7}{:^15}{:^15}{:^43}{:>11}{:>11}{:>11}\r\n'.format(
                            leitura['num_coletor'],
                            leitura['ordem'],
                            leitura['leitura'],
                            lm,
                            descricao,
                            '{:.2f}'.format(preco),
                            '{:.3f}'.format(leitura['pecas']),
                            '{:.2f}'.format(valor)
                        ))
                file.write('{:^9}{:^7}{:^15}{:^15}{:^43}{:>11}{:>11}{:>11}\r\n'.format(
                    '',
                    '',
                    '',
                    '',
                    '',
                    '',
                    '--------',
                    '--------',
                ))
                file.write('{:^9}{:^7}{:^15}{:^15}{:^43}{:>11}{:>11}{:>11}\r\n'.format(
                    '',
                    '',
                    '',
                    '',
                    '',
                    '',
                    '{:.3f}'.format(total_pecas),
                    '{:.2f}'.format(total_valor)
                ))
            win32api.ShellExecute(
                0,
                "print",
                filename,
                #
                # If this is None, the default printer will
                # be used anyway.
                #
                '/d:"%s"' % win32print.GetDefaultPrinter(),
                ".",
                0
            )
        self.secao_inicio.set('')
        self.secao_fim.set('')
        self.d.sync()

    def __init__(self, master=None):
        Frame.__init__(self, master)
        self.pack()
        self.d = shelve.open('db.txt')
        if 'pasta' not in self.d:
            self.definir_pasta()
        if 'num_coletores' not in self.d:
            self.d['num_coletores'] = 0
        if 'num_pecas' not in self.d:
            self.d['num_pecas'] = 0
        if 'num_secoes' not in self.d:
            self.d['num_secoes'] = 0
        if 'coletores' not in self.d:
            self.d['coletores'] = []
        if 'secoes' not in self.d:
            self.d['secoes'] = []
        if 'id_inventario' not in self.d:
            self.d['id_inventario'] = 1
        if 'id_base' not in self.d:
            self.d['id_base'] = 1
        if 'filial' not in self.d:
            self.d['filial'] = ''
        if 'data' not in self.d:
            self.d['data'] = ''
        if 'cadastro' not in self.d:
            self.d['cadastro'] = ''
        if 'servidor' not in self.d:
            self.d['servidor'] = '192.168.0.36'

        # self.lbl_servidor = Label(text='Servidor')
        # self.lbl_servidor.pack()
        #
        # self.servidor = StringVar(None)
        # self.servidor.set(self.d['servidor'])
        #
        # self.txt_servidor = Entry(bg='white', textvariable=self.servidor)
        # self.txt_servidor.pack()

        # self.lbl_id_inventario = Label(text='Id Inventário')
        # self.lbl_id_inventario.pack()
        #
        # self.id_inventario = StringVar(None)
        # self.id_inventario.set(self.d['id_inventario'])

        # self.txt_id_inventario = Entry(bg='white', textvariable=self.id_inventario)
        # self.txt_id_inventario.pack()

        self.lbl_pasta = Label(text='Pasta Inventário: {}'.format(self.d['pasta']))
        self.lbl_pasta.pack()

        self.lbl_cadastro = Label(text='Arquivo Cadastro: {}'.format(self.d['cadastro']))
        self.lbl_cadastro.pack()

        self.lbl_num_secoes = Label(text='Número de Seções: {}'.format(self.d['num_secoes']))
        self.lbl_num_secoes.pack()

        self.lbl_num_coletores = Label(text='Número de Coletores: {}'.format(self.d['num_coletores']))
        self.lbl_num_coletores.pack()

        self.lbl_num_pecas = Label(text='Número de Peças: {}'.format(self.d['num_pecas']))
        self.lbl_num_pecas.pack()

        self.lbl_id_base = Label(text='Id Base')
        self.lbl_id_base.pack()

        self.id_base = StringVar(None)
        self.id_base.set(self.d['id_base'])

        self.txt_id_base = Entry(bg='white', textvariable=self.id_base)
        self.txt_id_base.pack()

        self.lbl_filial = Label(text='Filial:')
        self.lbl_filial.pack()

        self.filial = StringVar(None)
        self.filial.set(self.d['filial'])

        self.txt_filial = Entry(bg='white', textvariable=self.filial)
        self.txt_filial.pack()

        # self.lbl_filial = Label(text='Filial: {}'.format(self.d['filial']))
        # self.lbl_filial.pack()

        self.lbl_data = Label(text='Data:')
        self.lbl_data.pack()

        self.data = StringVar(None)
        self.data.set(self.d['data'])

        self.txt_data = Entry(bg='white', textvariable=self.data)
        self.txt_data.pack()

        # self.lbl_data = Label(text='Data: {}'.format(self.d['data']))
        # self.lbl_data.pack()

        # self.lbl_data = Label(text='Data Inventário: {}'.format(self.d['data']))
        # self.lbl_data.pack()



        self.btn_quit = Button(self)
        self.btn_quit["text"] = "Fechar"
        self.btn_quit["fg"] = "red"
        self.btn_quit["command"] = self.quit

        self.btn_quit.pack({"side": "right"})

        self.btn_definir_pasta = Button(self)
        self.btn_definir_pasta["text"] = "Definir_Pasta",
        self.btn_definir_pasta["command"] = self.definir_pasta

        self.btn_definir_pasta.pack({"side": "left"})

        # self.btn_lista_inventarios = Button(self)
        # self.btn_lista_inventarios["text"] = "Lista_Inventários",
        # self.btn_lista_inventarios["command"] = self.lista_inventarios
        #
        # self.btn_lista_inventarios.pack({"side": "left"})
        #
        # self.btn_dados_inventario = Button(self)
        # self.btn_dados_inventario["text"] = "Pegar_Dados_Inventário",
        # self.btn_dados_inventario["command"] = self.dados_inventario
        #
        # self.btn_dados_inventario.pack({"side": "left"})

        self.btn_ler_cadastro = Button(self)
        self.btn_ler_cadastro["text"] = "Ler_Cadastro",
        self.btn_ler_cadastro["command"] = self.ler_cadastro

        self.btn_ler_cadastro.pack({"side": "left"})

        self.btn_atualizar = Button(self)
        self.btn_atualizar["text"] = "Atualizar",
        self.btn_atualizar["command"] = self.atualizar

        self.btn_atualizar.pack({"side": "left"})

        # self.lbl_secao_inicio = Label(text='Seção Inicial')
        # self.lbl_secao_inicio.pack()
        #
        # self.secao_inicio = StringVar(None)
        #
        # self.txt_secao_inicio = Entry(bg='green', textvariable=self.secao_inicio)
        # self.txt_secao_inicio.pack()
        #
        # self.lbl_secao_fim = Label(text='Seção Final')
        # self.lbl_secao_fim.pack()
        #
        # self.secao_fim = StringVar(None)
        #
        # self.txt_secao_fim = Entry(bg='red', textvariable=self.secao_fim)
        # self.txt_secao_fim.pack()

        # self.btn_imprimir_secao = Button(self)
        # self.btn_imprimir_secao["text"] = "Imprimir",
        # self.btn_imprimir_secao["command"] = self.imprimir_secao
        # self.btn_imprimir_secao.pack()


root = Tk()
# root.attributes('-fullscreen', True)
w, h = root.winfo_screenwidth(), root.winfo_screenheight()
root.geometry("%dx%d+0+0" % (w, h))
app = Application(master=root)

app.mainloop()
root.destroy()
